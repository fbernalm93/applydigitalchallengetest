# syntax=docker/dockerfile:1
FROM maven
USER root
WORKDIR /home
COPY Apply_Digital_Challenge/ Apply_Digital_Challenge/
WORKDIR Apply_Digital_Challenge/
#RUN mvn package
#WORKDIR /home/Apply_Digital_Challenge/target/
EXPOSE 8081 
#CMD ["java", "-jar", "Apply_Digital_Challenge-1.0.0-DEV.jar"]
CMD ["mvn", "spring-boot:run"]
#RUN touch test
#CMD ["tail", "-f", "test"]