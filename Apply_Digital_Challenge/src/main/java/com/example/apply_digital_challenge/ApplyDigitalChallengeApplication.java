package com.example.apply_digital_challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.example.apply_digital_challenge", "hackerJavaNews.blog.utils"})
public class ApplyDigitalChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApplyDigitalChallengeApplication.class, args);
    }

}
