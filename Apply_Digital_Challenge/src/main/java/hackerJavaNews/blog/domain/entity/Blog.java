package hackerJavaNews.blog.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "blog")
public class Blog {
    @Id
    @Column(name = "story_id")
    private Long story_id;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "title")
    private String title;
    @Column(name = "url")
    private String url;
    @Column(name = "author")
    private String author;
    @Column(name = "points")
    private String points;
    @Column(name = "story_text")
    private String story_text;
    @Column(name = "comment_text")
    private String comment_text;
    @Column(name = "num_comments")
    private int num_comments;
    @Column(name = "story_title")
    private String story_title;
    @Column(name = "story_url")
    private String story_url;
    @Column(name = "parent_id")
    private Long parent_id;
    @Column(name = "created_at_i")
    private Long created_at_i;
}
