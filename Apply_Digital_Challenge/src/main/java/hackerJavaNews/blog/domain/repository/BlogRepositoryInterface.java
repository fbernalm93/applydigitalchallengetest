package hackerJavaNews.blog.domain.repository;

import hackerJavaNews.blog.domain.entity.Blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepositoryInterface extends JpaRepository<Blog,String> {
}
