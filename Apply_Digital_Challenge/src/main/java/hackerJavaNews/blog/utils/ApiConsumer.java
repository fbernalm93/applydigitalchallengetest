package hackerJavaNews.blog.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hackerJavaNews.blog.domain.entity.Blog;
import hackerJavaNews.blog.domain.repository.BlogRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Component
public class ApiConsumer {
    public BlogRepositoryInterface blogRepository;
//    @Scheduled(fixedRate = 3600000) // Execute to (3600000 ms) (each hour)

//    @Scheduled(fixedRate = 60000) // Execute to (3600000 ms) (each minute)
//    @Scheduled(cron = "0 * * * * *")
    @Scheduled(fixedRate = 30000) // Ejecutar cada 30 segundos
    public void consumeApi() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://hn.algolia.com/api/v1/search_by_date?query=java";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode root = objectMapper.readTree(response.getBody());
        JsonNode hits = root.path("hits");

        List<Blog> newsList = new ArrayList<>();

        for (JsonNode hit : hits) {

            Blog news = new Blog();

            news.setStory_id(hit.path("story_id").asLong());
            news.setParent_id(hit.path("parent_id").asLong());
            news.setCreated_at_i(hit.path("created_at_i").asLong());

            news.setCreatedAt(new Date(hit.path("created_at").asLong() * 1000));

            news.setTitle(hit.path("title").asText());
            news.setUrl(hit.path("url").asText());
            news.setAuthor(hit.path("author").asText());
            news.setPoints(hit.path("points").asText());
            news.setStory_text(hit.path("story_text").asText());
            news.setComment_text(hit.path("comment_text").asText());
            news.setStory_title(hit.path("story_title").asText());
            news.setStory_url(hit.path("story_url").asText());

            news.setNum_comments(hit.path("num_comments").asInt());

            newsList.add(news);
        }
        blogRepository.saveAll(newsList);
    }
}
