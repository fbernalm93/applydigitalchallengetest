create table person(
   id varchar(10) primary key,
   name varchar(100) not null,
   lastname varchar(100) not null,
   email varchar(75) not null,
   birthdate date,
   address varchar(100),
   phonenumber varchar(10),
   isVaccinated boolean
);

create table userApp(
   username varchar(50) primary key,
   password varchar(100) not null,
	role varchar(20) not null
);